using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffReciver : MonoBehaviour
{
    private List<BuffOption> buffs;
    private void Start()
    {
        GameManager.Instance.buffContainer.Add(gameObject, this);
        buffs = new List<BuffOption>();
    }
    public void AddBuff(BuffOption buff)
    {
        if(!buffs.Contains(buff))
        {
            buffs.Add(buff);
        }
    }
    public void RemoveBuff(BuffOption buff)
    {
        if(buffs.Contains(buff))
        {
            buffs.Remove(buff);
        }
    }
}
