using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class PlayerInventoty : MonoBehaviour
{
    public int coinsCount;
    public int kitCount;
    [SerializeField] private Text coinsText;
    [SerializeField] private Text kitText;
    private void Start()
    {
        coinsText.text = coinsCount.ToString();
        kitText.text= kitCount.ToString();
    }
    public void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.coinContainer.ContainsKey(col.gameObject))//������� ���������� ������� � ���� ���������(�����)
        {
            coinsCount++;//������� ����� 
            coinsText.text =""+coinsCount;
            var coin = GameManager.Instance.coinContainer[col.gameObject];//������ � ������� � �������������
            coin.StardDestroy();//����� ������ ����������� ������                                                                      
        }
        if (GameManager.Instance.healingContainer.ContainsKey(col.gameObject))
        {
            kitCount++;            
            var heal = GameManager.Instance.healingContainer[col.gameObject];
            heal.StardDestroy();
        }       
    }  
}

