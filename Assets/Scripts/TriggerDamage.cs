using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TriggerDamage : MonoBehaviour
{
    private IObjectDestroyer destroyer;
    public int damage;
    public bool isDestroyAfterCol;
    private GameObject parent;    
    public GameObject Parent
    {
        get { return parent;}
        set { parent = value;}
    }
    public void Init(IObjectDestroyer destroyer)
    {
        this.destroyer = destroyer;        
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject == parent)
            return;
        
        if (GameManager.Instance.healthContainer.ContainsKey(col.gameObject))
        {
            var health = GameManager.Instance.healthContainer[col.gameObject];
            health.Hit(damage);
        
           if (isDestroyAfterCol)
           {
                if (destroyer == null)
                {
                    Destroy(gameObject);
                }
                else
                {
                    destroyer.Destroy(gameObject);
                }
           } 
        }
    }    
}
public interface IObjectDestroyer
{
    void Destroy(GameObject gameObject);
}
