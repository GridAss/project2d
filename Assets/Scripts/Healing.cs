using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healing : MonoBehaviour
{
    [SerializeField] private Animator animator; 
    
    void Start()
    {
        GameManager.Instance.healingContainer.Add(gameObject, this);
    }

    public void StardDestroy()
    {
        animator.SetTrigger("StartDestroy");
    }
    public void EndDestroy()
    {
        Destroy(gameObject);
    }
}
