using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class HitDamage : MonoBehaviour
{
    public int damage;    
    public Animator animator;
    private Health health;
    public float direction;
    public SpriteRenderer spriteRenderer;
    public void OnCollisionStay2D(Collision2D collision)
    {
        health = collision.gameObject.GetComponent<Health>();
        //����� ��������� �������� � �������� �������
        if (health != null)//�������� ������� �������� � �������
        {
            direction = (collision.transform.position - transform.position).x;
            //�������� ������� ����� ������������ ������
            animator.SetFloat("Direction",Mathf.Abs(direction));//����� ������� ��������� �����
        }        
    }
    public void SetDamage()
    {
        if (health != null)//�������� ������� �������� � �������
        {
            health.Hit(damage);//��������� ����� 
        }
        health = null;//��������� ��������,��� ���������� ���������� ��������� �����
        direction = 0;//�������������� ��������� �������� ��������� ����� 
        animator.SetFloat("Direction", 0f);//������� ��������� �������� ��������� �����
    }
}


