using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Health : MonoBehaviour
{
    [SerializeField] public int health;
    [SerializeField] public int healsBonus;
    public void Start()
    {
        GameManager.Instance.healthContainer.Add(gameObject, this);
    }
    public void Hit(int damage)
    {
        health -= damage;//��������� �����        
        if (health <= 0)//������� ��� ������� �������� ����������� 
        {
            Destroy(gameObject);//����������� �������
        }
    }
    public void HealingBonus(int healthBonus)
    {        
            health += healthBonus;//��������� ��������(�������)
        if (health > 100) //������� ������������� ���������� �������� 
        {
            health = 100;//����������� �������� ������������ ��������
        }       
    }
}
 