using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Player : MonoBehaviour
{ public float speed;
    public float force;
    public float minimalHeight;
    public bool cheatMode;
    public GroundPlace groundPlace;
    private Vector3 direction;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    public bool isJumping;
    [SerializeField] private Transform arrowSpawn;
    public Rigidbody2D _rigidbody;
    [SerializeField] private Arrow arrow;
    [SerializeField] private float shootForce;
    private Arrow flayArrow;
    private List<Arrow> arrowPool;
    [SerializeField] private int arrowCount;
    [SerializeField] private bool isCooldown;
    [SerializeField] private int cooldown;    
    private void Start()
    {
        arrowPool = new List<Arrow>();
        for (int i = 0; i < arrowCount; i++)
        {                       
                var arrowTemp = Instantiate(arrow, arrowSpawn);
                arrowPool.Add(arrowTemp);
                arrowTemp.gameObject.SetActive(false);            
        }
    }
    void Update()
    {
        #region Down
        animator.SetBool("GroundPlace", groundPlace.isground);//������� ���������� �� �����(��� ��������)
        if (!isJumping && !groundPlace.isground)//������� ������������ �������� �������
        {
            animator.SetTrigger("StartFall");//������ �� �������� �������
        }
        isJumping = isJumping && !groundPlace.isground;//������� �������� ������ � �������� ���������� �� �����
        direction = Vector3.zero;//��������� ������� ���� �������� ��������� ���������
        #endregion
        #region Moving
        if (Input.GetKey(KeyCode.A))//���� ������ ������ �
        {
            direction = Vector3.left;//������������ ���� � ����
        }
        if (Input.GetKey(KeyCode.D))//���� ������ ������ D
        {
            direction = Vector3.right;//������������ ���� � �����
        }
        direction *= speed;//����������� ���� � �������� 
        direction.y = _rigidbody.velocity.y;//���������� ���� �� Y
        _rigidbody.velocity = direction;//������������ �������� � ����������� ��������
        #endregion
        #region Jump
        if (Input.GetKeyDown(KeyCode.Space) && groundPlace.isground)//���� ����� ������ � ������ ��������� �� �����         
        {
            _rigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);//������������ ���� ��� ������
            animator.SetTrigger("StartJump");//������ ��� �������� ������
        }
        #endregion       
        #region Flip
        if (direction.x > 0)//����� ������� ��������
        {
            spriteRenderer.flipX = false;//��������� �������
        }
        if (direction.x < 0)//����� ������� ��������
        {
            spriteRenderer.flipX = true;//��������� �������
        }
        //������� ���������� ������� ������������ �������
        #endregion
        animator.SetFloat("Speed", Mathf.Abs(direction.x));//������� �������� �������� ������� � ������
        CheckFall();//����� ����� �������� ���������� �� ���� ����������� �������
        CheckShoot();//����� ������ �������� ���������� ��������
        

    }
    void CheckShoot()
    {
        if (Input.GetMouseButtonDown(0) && (isCooldown == false))//������� ������� ������ ���� � ����������� 
        {
            animator.SetTrigger("StartShoot");//������ �� ����� ������ �������� ��������
            isCooldown = true;//������� �� �����������                                 
        }
    }
    public void InitArrow()
    {
        flayArrow = GetArrowFromPool();
        flayArrow.SetImpuls(Vector2.right, 0, this);
    }
    private void Shot()
    {
        flayArrow.SetImpuls(Vector2.right, spriteRenderer.flipX ?
            -force * shootForce : force * shootForce, this);
        StartCoroutine(Cooldown());//������ �������� ������� ����������
    }
    private IEnumerator Cooldown()//����� �������� ������� ����������
    {
        isCooldown = true;
        yield return new WaitForSeconds(cooldown);
        isCooldown = false;
    }
    private Arrow GetArrowFromPool()
    {
        if(arrowPool.Count>0)
        {
            var arrowTemp = arrowPool[0];
            arrowPool.Remove(arrowTemp);
            arrowTemp.gameObject.SetActive(true);
            arrowTemp.transform.parent = null;
            arrowTemp.transform.position = arrowSpawn.transform.position;
            return arrowTemp;
        }
        return Instantiate
               (arrow, arrowSpawn.transform.position, Quaternion.identity);
    }
    public void ReturnArrowFromPool(Arrow arrowTemp)
    {
        if (!arrowPool.Contains(arrowTemp))
        {
            arrowPool.Add(arrowTemp);
        }
        arrowTemp.transform.parent = arrowSpawn;
        arrowTemp.transform.position = arrowSpawn.transform.position;
        arrowTemp.gameObject.SetActive(false);        
    }
    void CheckFall()
    {
        if (transform.position.y < minimalHeight && cheatMode)//�������� ������� ���������� ���� ����������� ����� � ��������� �����
        {
            _rigidbody.velocity = new Vector2(0, 0);//���������� ��������� �������
            transform.position = new Vector2(0, 0);//������� �� ��������� �������
        }
        else if (transform.position.y < minimalHeight) //�������� ������� ���������� ���� ����������� ����� 
        {
            Destroy(gameObject);//����������� �������
        }
    }     
}  

    

 