using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager Instance { get; private set; }
    #endregion
    public Dictionary<GameObject, Health> healthContainer;
    public Dictionary<GameObject,Coin> coinContainer;
    public Dictionary<GameObject, BuffReciver> buffContainer;
    public Dictionary<GameObject, Healing> healingContainer;
    private void Awake()
    {
        Instance = this;
        healthContainer = new Dictionary<GameObject, Health>();//�������� ������� ��������
        coinContainer = new Dictionary<GameObject, Coin>();//�������� ������� �����
        buffContainer = new Dictionary<GameObject, BuffReciver>();//�������� ������� �����
        healingContainer = new Dictionary<GameObject, Healing>();//�������� ������� �����
    }
    public void OnclickPause()
    {
        if (Time.timeScale > 0)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
