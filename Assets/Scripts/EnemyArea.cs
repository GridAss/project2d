using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArea : MonoBehaviour
{
    public GameObject leftBorder;
    public GameObject rightBorder;
    public bool isPlace;
    [SerializeField] public float speed;
    public Rigidbody2D rig;
    public GroundPlace groundPlace;
    public SpriteRenderer spriteRenderer;
    public HitDamage hitDamage;    
    void Update()
    {
        if(groundPlace.isground)//�������� ���������� ������� �� �����
        {
            if (transform.position.x > rightBorder.transform.position.x || hitDamage.direction < 0)
            {
                isPlace = false;
            }
            if (transform.position.x < leftBorder.transform.position.x || hitDamage.direction > 0)
            {
                isPlace = true;
            }
            //�������� ���������� ������� ������ ������ ����������� � ��������� ������������ ������
            rig.velocity = isPlace ? Vector2.right : Vector2.left;
            //������� ������ ����������� �������� ������� ������������ ������
            rig.velocity *= speed;//��������� �������� ����������� (��������)
        }
        #region Flip
        if (rig.velocity.x < 0)
        {
            spriteRenderer.flipX = false;
        }
        if (rig.velocity.x > 0)
        {
            spriteRenderer.flipX = true;
        }
        //������� ���������� ������� ������������ �������
        #endregion
    }
}
