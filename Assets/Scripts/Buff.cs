using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff : MonoBehaviour
{
    [SerializeField] private BuffOption buff;
    private void OnTriggerEnter2D(Collider2D col)
    {
      if(GameManager.Instance.buffContainer.ContainsKey(col.gameObject))
      {
            var reciver = GameManager.Instance.buffContainer[col.gameObject];
            reciver.AddBuff(buff);
      }
    }
    private void OnTriggerExit2D(Collider2D col)
    {
        if (GameManager.Instance.buffContainer.ContainsKey(col.gameObject))
        {
            var reciver = GameManager.Instance.buffContainer[col.gameObject];
            reciver.RemoveBuff(buff);
        }
    }
}
[System.Serializable]
public class BuffOption
{
    public BuffType type;
    public float sumBuff;
    public float przBuff;
}
public enum BuffType:byte
{
    Damage,Force
}
